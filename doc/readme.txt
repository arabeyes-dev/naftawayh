Naftawayh: Arabic word tagger
=============================
Arabic Word Tagger: This class can identify the typoe of word (noun, verb, prepostion):
Naftawayh is a python library for Arabic word tagging (word classification)  into types (nouns, verbs, stopwords), which is useful in language processing, especially for text mining.
Naftawayh works according to the Arabic word structure,  and the ability to guess the word class,  through certain signs. For example, a word which ends Teh Marbuta, is a noun. Hamza Below Alef, class the word as a noun. 
We can identify many kins of words, by patterns especially for verbs in present tense and defined words.

Applications:
------------- 
 - Text mining.
 - Text summarizing 
 - Sentences identification
 - Grammar. analysis
 - Morphological analysis acceleration.
 - Extraction of  ngrams.



To test :
=========
You can test this program online at http://adawaty.appspot.com

You can see a sample of naftawayh script in 
	test_wordtag_file.py
	
This program is licensed under the GPL License